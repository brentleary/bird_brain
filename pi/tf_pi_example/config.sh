# sudo raspi-config
# Setup wifi, enable camera, ssh, vnc
# Set gpu memory to 256

# Setup git and set hostname
git config --global user.email "leary.brent@gmail.com"
git config --global user.name "Brent Leary"
#hostnamectl set-hostname birdbrain

#Setup passwordless ssh
#from admin powershell
set-executionpolicy unrestricted
# unzip OpenSSH.7z to program files and then run ./install-sshd.ps1 from powershell
Set-Service ssh-agent -StartupType Automatic
Start-Service ssh-agent
cd ~\.ssh
ssh-keygen
ssh-add id_rsa
cat ~/.ssh/id_rsa.pub | ssh pi@birdbrain4 'mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys'
ssh pi@birdbrain4

# Tensorflow and dependencies to run perf example
sudo apt-get install -y libatlas-base-dev picamera
sudo apt-get install -y python3-pip
pip3 install --upgrade setuptools
wget https://github.com/PINTO0309/Tensorflow-bin/raw/master/tensorflow-1.15.0-cp37-cp37m-linux_armv7l.whl
sudo pip3 install tensorflow-1.15.0-cp37-cp37m-linux_armv7l.whl
sudo apt-get install -y python3-opencv
pip3 install Pillow
pip3 install numpy

# https://github.com/tensorflow/examples/tree/master/lite/examples/object_detection/raspberry_pi
pip3 install numpy
pip3 install picamera
pip3 install Pillow