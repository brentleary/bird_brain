pip3 install --user -U pip testresources setuptools
pip3 install tensorflow-datasets opencv-python numpy matplotlib tensorflow_hub virtualenv virtualenvwrapper matplotlib Pillow
pip3 install --user -U numpy==1.16.1 future==0.17.1 mock==3.0.5 h5py==2.10.0 keras_preprocessing==1.1.0 keras_applications==1.0.8 gast==0.3.3 futures protobuf pybind11
pip3 install --user --pre tensorflow
