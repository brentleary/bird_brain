import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import json # json.dumps(parsed, indent=4, sort_keys=True))
import pprint
pp = pprint.PrettyPrinter(indent=4)

import tensorflow_datasets as tfds
tfds.list_builders()

ds, ds_info = tfds.load('mnist', split='train', with_info=True) #('caltech_birds2011', shuffle_files=True, with_info=True, split=['train+test[:80%]', 'test'])
# image, label = tfds.as_numpy(tfds.load('caltech_birds2011', batch_size=-1, as_supervised=True,))
# print(type(image), image.shape)

#assert isinstance(ds, tf.data.Dataset)
# fig = tfds.show_examples(ds, ds_info, image_key="image")
# ds = ds.take(1)  # Only take a single example
# for example in ds_cal:  # example is `{'image': tf.Tensor, 'label': tf.Tensor}`
#   print(list(example.keys()))
#   image = example["image"]
#   label = example["label"]
#   print(image.shape, label)

pp.pprint(ds)
pp.pprint(ds_info)
pp.pprint(list(ds_info.splits.keys()))
pp.pprint(ds_info.splits['train'].num_examples)
pp.pprint(ds_info.splits['train'].filenames)
pp.pprint(ds_info.splits['train'].num_shards)
pp.pprint(ds_info.splits['train[15%:75%]'].num_examples)
pp.pprint(ds_info.splits['train[15%:75%]'].file_instructions)

pp.pprint(ds_info.features)
pp.pprint(ds_info.features["label"].num_classes)
pp.pprint(ds_info.features["label"].names)
pp.pprint(ds_info.features["label"].int2str(7))  # Human readable version (8 -> 'cat')
pp.pprint(ds_info.features["label"].str2int('7'))
pp.pprint(ds_info.features.shape)
pp.pprint(ds_info.features.dtype)
pp.pprint(ds_info.features['image'].shape)
pp.pprint(ds_info.features['image'].dtype)


for image, label in tfds.as_numpy(ds):
  print(type(image), type(label), label)

for image, label in ds:  # example is (image, label)
  print(image.shape, label)

