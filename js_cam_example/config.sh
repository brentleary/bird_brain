# Setup git and set hostname
git config --global user.email "leary.brent@gmail.com"
git config --global user.name "Brent Leary"
#hostnamectl set-hostname birdbrainV

#Setup passwordless ssh
#from admin powershell
#set-executionpolicy unrestricted

# unzip OpenSSH.7z to program files and then run ./install-sshd.ps1 from powershell
#Set-Service ssh-agent -StartupType Automatic
#Start-Service ssh-agent
#cd ~\.ssh
#ssh-keygen
#ssh-add id_rsa
#cat ~/.ssh/id_rsa.pub | ssh pi@birdbrainV 'mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys'
#ssh pi@birdbrainV


# Installs vscode
# Start an elevated session
#sudo -s
# Install the community repo key
#apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 0CC3FD642696BFC8
# Run the installation script
#. <( wget -O - https://code.headmelted.com/installers/apt.sh )

# https://codeburst.io/installing-reactjs-and-creating-your-first-application-d437706498ed
sudo apt-get install -y nodejs npm build-essential
sudo npm install -g create-react-app

git clone https://github.com/tombaranowicz/SurveillanceCameraJS