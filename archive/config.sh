# sudo raspi-config
# Setup wifi, enable camera, ssh, vnc
# Set gpu memory to 256

# Setup git and set hostname
git config --global user.email "leary.brent@gmail.com"
git config --global user.name "Brent Leary"
#hostnamectl set-hostname birdbrain
sudo apt install git-lfs

#Setup passwordless SsH
# On the pi
#mkdir ~/.ssh
#touch authorized_keys
#ssh-keygen

#From windows10 admin powershell
Set-Service ssh-agent -StartupType Automatic
Start-Service ssh-agent
cd ~\.ssh
ssh-keygen
ssh-add id_rsa
cat ~/.ssh/id_rsa.pub | ssh pi@birdbrain4 'mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys'


# Installs motion which is able to detect when movement occurs but processing utilization is to high for pi zero
#sudo apt install -y autoconf automake build-essential pkgconf libtool git libzip-dev libjpeg-dev gettext libmicrohttpd-dev libavformat-dev libavcodec-dev libavutil-dev libswscale-dev libavdevice-dev default-libmysqlclient-dev libpq-dev libsqlite3-dev libwebp-dev
#sudo wget https://github.com/Motion-Project/motion/releases/download/release-4.3.1/pi_buster_motion_4.3.1-1_armhf.deb
#sudo dpkg -i pi_buster_motion_4.3.1-1_armhf.deb

# Monitors network throughput
# sudo apt install -y vnstat
# vnstat --iface wlan0 --live

# uv4l low latency pi camera drivers
sudo cp ./etc/apt/sources.list /etc/apt/
curl http://www.linux-projects.org/listing/uv4l_repo/lpkey.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install -y uv4l uv4l-raspicam uv4l-server uv4l-demos uv4l-raspicam-extras #u4vl-mjpegstream
#sudo service uv4l_raspicam status/start/stop/restart
#uv4l --config-file=/etc/uv4l/uv4l-raspicam.conf --driver-config-file=/etc/uv4l/uv4l-raspicam.conf --enable-server on
#pkill uv4l

# Install other apps
# Tensor flow lite https://www.tensorflow.org/lite/guide/python
# iperf3 test network performance between 2 computers
sudo apt-get install -y python3 python3-pip iperf3 ntopng


#git clone -b v2.1.0 https://github.com/PINTO0309/Tensorflow-bin.git
pip3 install ./Tensorflow-bin/tensorflow-2.1.0-cp37-cp37m-linux_aarch64.whl
wget https://github.com/PINTO0309/Tensorflow-bin/raw/master/tensorflow-1.15.0-cp37-cp37m-linux_aarch64.whl
pip3 install tensorflow-1.15.0-cp37-cp37m-linux_aarch64.whl

# Temp and CPU monitoring commands
#vcgencmd measure_temp
#sar -u 2 # every two seconds show usage

# Copy config files into distro from our repo
#cp -r ./etc /etc

# Add to ~./bashrc in order to use workon and other commands
source /home/pi/.local/bin/virtualenvwrapper.sh

# Latest ntopng
git clone https://github.com/ntop/ntopng.git
cd ntopng; ./autogen.sh; ./configure; make; make install


# Nodejs rtsp server
# https://codecalamity.com/raspberry-pi-hardware-accelerated-h264-webcam-security-camera/
curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
sudo apt install nodejs
sudo npm install -g coffeescript
git clone https://github.com/iizukanao/node-rtsp-rtmp-server.git --depth 1
cd node-rtsp-rtmp-server
npm install -d


# Didn't work
#sudo apt-get install -y cmake liblog4cpp5-dev libv4l-dev
#git clone https://github.com/mpromonet/v4l2rtspserver.git
#cd v4l2rtspserver/
#cmake .
#make
#sudo make install

sudo apt-get install -y python3-pip
sudo apt-get install -y libatlas-base-dev
pip3 install --upgrade setuptools
wget https://github.com/PINTO0309/Tensorflow-bin/raw/master/tensorflow-1.15.0-cp37-cp37m-linux_armv7l.whl
sudo pip3 install tensorflow-1.15.0-cp37-cp37m-linux_armv7l.whl
sudo apt-get install -y python3-opencv
pip3 install Pillow
pip3 install numpy

#
# sudo apt-get install -y libhdf5-dev libc-ares-dev libeigen3-dev gcc gfortran python-dev libgfortran5 \
#                           libatlas3-base libatlas-base-dev libopenblas-dev libopenblas-base libblas-dev \
#                           liblapack-dev cython libatlas-base-dev openmpi-bin libopenmpi-dev python3-dev
# sudo pip3 install keras_applications==1.0.8 --no-deps
# sudo pip3 install keras_preprocessing==1.1.0 --no-deps
# sudo pip3 install h5py==2.9.0
# sudo pip3 install pybind11
# pip3 install -U --user six wheel mock
# wget https://github.com/PINTO0309/Tensorflow-bin/raw/master/tensorflow-2.1.0-cp37-cp37m-linux_armv7l.whl
# sudo pip3 uninstall tensorflow
# sudo -H pip3 install tensorflow-2.1.0-cp37-cp37m-linux_armv7l.whl
#
# sudo apt-get install -y libhdf5-dev libc-ares-dev libeigen3-dev gcc gfortran python-dev libgfortran5 \
#                           libatlas3-base libatlas-base-dev libopenblas-dev libopenblas-base libblas-dev \
#                           liblapack-dev cython openmpi-bin libopenmpi-dev libatlas-base-dev python3-dev
#
# sudo pip3 install keras_applications==1.0.8 --no-deps
# sudo pip3 install keras_preprocessing==1.1.0 --no-deps
# sudo pip3 install h5py==2.9.0
# sudo pip3 install pybind11
# pip3 install -U --user six wheel mock
# sudo pip3 uninstall tensorflow
# wget https://github.com/PINTO0309/Tensorflow-bin/raw/master/tensorflow-1.15.0-cp37-cp37m-linux_armv7l.whl
# sudo pip3 install tensorflow-1.15.0-cp37-cp37m-linux_armv7l.whl
