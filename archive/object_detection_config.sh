#!/usr/bin/env bash
#https://github.com/EdjeElectronics/TensorFlow-Object-Detection-on-the-Raspberry-Pi
echo
echo 'INSTALLING DEPENDENCIES | TODO REMOVE OPENCV'
sudo apt update
sudo apt -y dist-upgrade
sudo apt install -y libatlas-base-dev
sudo apt-get install -y libjpeg-dev libtiff5-dev libjasper-dev libpng12-dev
sudo apt-get install -y libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
sudo apt-get install -y libxvidcore-dev libx264-dev
sudo apt-get install -y qt4-dev-tools libatlas-base-dev
sudo apt-get install -y protobuf-compiler

echo
echo 'CHECKING PROTOC VERSION'
protoc --version

echo
echo 'INSTALLING TENSORFLOW'
pip3 install pillow lxml jupyter matplotlib cython opencv-python
wget https://github.com/PINTO0309/Tensorflow-bin/raw/master/tensorflow-1.15.0-cp37-cp37m-linux_aarch64.whl
pip3 install tensorflow-1.15.0-cp37-cp37m-linux_aarch64.whl

echo
echo 'CREATING TENSORFLOW1 AND DOWNLOADING MODELS'
[ ! -d "../tensorflow1" ] && mkdir ../tensorflow1
cd ../tensorflow1
[ -d "../tensorflow1" ] && sudo rm -rf models
git clone --depth 1 https://github.com/tensorflow/models.git

#Add the line to the end of bashrc if it doesn't already exist
echo
echo 'ADDING PYTHONPATH TO BASHRC'
last_line=$( tail -n 1 ~/.bashrc )
[! "$last_line" = 'export PYTHONPATH=$PYTHONPATH:/home/pi/tensorflow1/models/research:/home/pi/tensorflow1/models/research/slim'] && echo 'export PYTHONPATH=$PYTHONPATH:/home/pi/tensorflow1/models/research:/home/pi/tensorflow1/models/research/slim' | sudo tee -a ~/.bashrc
#Adds to this bash shell now
export PYTHONPATH=$PYTHONPATH:/home/pi/tensorflow1/models/research:/home/pi/tensorflow1/models/research/slim

echo
echo 'DOWNLOADING ssdlite_mobilenet_v2_coco_2018_05_09'
cd /home/pi/tensorflow1/models/research/object_detection
wget http://download.tensorflow.org/models/object_detection/ssdlite_mobilenet_v2_coco_2018_05_09.tar.gz
tar -xzvf ssdlite_mobilenet_v2_coco_2018_05_09.tar.gz

echo
echo 'BUILDING MODEL PROTOS WITH PROTOC'
cd /home/pi/tensorflow1/models/research
protoc object_detection/protos/*.proto --python_out=.
