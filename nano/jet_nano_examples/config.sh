# Setup git and set hostname
#git config --global user.email "leary.brent@gmail.com"
#git config --global user.name "Brent Leary"
#hostnamectl set-hostname birdbrainV

#Setup passwordless ssh
#from admin powershell
#set-executionpolicy unrestricted

# unzip OpenSSH.7z to program files and then run ./install-sshd.ps1 from powershell
#Set-Service ssh-agent -StartupType Automatic
#Start-Service ssh-agent
#cd ~\.ssh
#ssh-keygen
#ssh-add id_rsa
#cat ~/.ssh/id_rsa.pub | ssh pi@birdbrainV 'mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys'
#ssh pi@birdbrainV


# Installs vscode
# Start an elevated session
#sudo -s
# Install the community repo key
#apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 0CC3FD642696BFC8
# Run the installation script
#. <( wget -O - https://code.headmelted.com/installers/apt.sh )

# https://github.com/dusty-nv/jetson-inference/blob/master/docs/building-repo-2.md
# sudo apt install -y libpython3-dev python3-numpy xrdp v4l-utils
# sudo apt install -y git cmake libpython3-dev python3-numpy
# git clone --recursive https://github.com/dusty-nv/jetson-inference
 cd jetson-inference
 mkdir build
 cd build
 cmake ../
 make -j$(nproc)
 sudo make install
 sudo ldconfig

# # Tensorflow
# # https://docs.nvidia.com/deeplearning/frameworks/install-tf-jetson-platform/index.html
# sudo apt-get update
# sudo apt-get install -y libhdf5-serial-dev hdf5-tools libhdf5-dev zlib1g-dev zip libjpeg8-dev liblapack-dev libblas-dev gfortran
# # Install and upgrade pip3.
# sudo apt-get install -y python3-pip
# sudo pip3 install -U pip testresources setuptools
# # Install the Python package dependencies.
# sudo pip3 install -U numpy==1.16.1 future==0.17.1 mock==3.0.5 h5py==2.9.0 keras_preprocessing==1.0.5 keras_applications==1.0.8 gast==0.2.2 futures protobuf pybind11

# # Installs JP 44 TF
# sudo pip3 install --pre --extra-index-url https://developer.download.nvidia.com/compute/redist/jp/v44 tensorflow

# # Jetson stats
# # https://github.com/rbonghi/jetson_stats
# sudo -H pip install -U jetson-stats