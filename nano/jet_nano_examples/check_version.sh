#
# check L4T version
#
function check_L4T_version()
{
	JETSON_L4T_STRING=$(head -n 1 /etc/nv_tegra_release)

	if [ -z $2 ]; then
		echo "$LOG reading L4T version from \"dpkg-query --show nvidia-l4t-core\""

		JETSON_L4T_STRING=$(dpkg-query --showformat='${Version}' --show nvidia-l4t-core)
		local JETSON_L4T_ARRAY=(${JETSON_L4T_STRING//./ })	

		#echo ${JETSON_L4T_ARRAY[@]}
		#echo ${#JETSON_L4T_ARRAY[@]}

		JETSON_L4T_RELEASE=${JETSON_L4T_ARRAY[0]}
		JETSON_L4T_REVISION=${JETSON_L4T_ARRAY[1]}
	else
		echo "$LOG reading L4T version from /etc/nv_tegra_release"

		JETSON_L4T_RELEASE=$(echo $JETSON_L4T_STRING | cut -f 2 -d ' ' | grep -Po '(?<=R)[^;]+')
		JETSON_L4T_REVISION=$(echo $JETSON_L4T_STRING | cut -f 2 -d ',' | grep -Po '(?<=REVISION: )[^;]+')
	fi

	JETSON_L4T_VERSION="$JETSON_L4T_RELEASE.$JETSON_L4T_REVISION"
	echo "$LOG Jetson BSP Version:  L4T R$JETSON_L4T_VERSION"

	if [ $JETSON_L4T_RELEASE -lt 32 ]; then
		dialog --backtitle "$APP_TITLE" \
		  --title "PyTorch Automated Install requires JetPack ≥4.2" \
		  --colors \
		  --msgbox "\nThis script to install PyTorch from pre-built binaries\nrequires \ZbJetPack 4.2 or newer\Zn (L4T R32.1 or newer).\n\nThe version of L4T on your system is:  \ZbL4T R${JETSON_L4T_VERSION}\Zn\n\nIf you wish to install PyTorch for training on Jetson,\nplease upgrade to JetPack 4.2 or newer, or see these\ninstructions to build PyTorch from source:\n\n          \Zbhttps://eLinux.org/Jetson_Zoo\Zn\n\nNote that PyTorch isn't required to build the repo,\njust for re-training networks onboard your Jetson.\nYou can proceed following Hello AI World without it,\nexcept for the parts on Transfer Learning with PyTorch." 20 60

		clear
		echo " "
		echo "[jetson-inference]  this script to install PyTorch from pre-built binaries"
		echo "                    requires JetPack 4.2 or newer (L4T R32.1 or newer).  "
		echo "                    the version of L4T on your system is:  L4T R${JETSON_L4T_VERSION}"
		echo " "
		echo "                    if you wish to install PyTorch for training on Jetson,"
		echo "                    please upgrade to JetPack 4.2 or newer, or see these"
		echo "                    instructions to build PyTorch from source:"
		echo " "
		echo "                        > https://eLinux.org/Jetson_Zoo"
		echo " "
		echo "                    note that PyTorch isn't required to build the repo,"
		echo "                    just for re-training networks onboard your Jetson."
		echo " "
		echo "                    you can proceed following Hello AI World without it,"
		echo "                    except for the parts on Transfer Learning with PyTorch."

		exit_message 1
	fi
}


# check L4T version
check_L4T_version