#!/usr/bin/python
#
# Copyright (c) 2019, NVIDIA CORPORATION. All rights reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#

import jetson.inference
import jetson.utils

NETWORK = "ssd-mobilenet-v2"

net = jetson.inference.detectNet(NETWORK, threshold=0.5)
#camera = jetson.utils.gstCamera(1280, 720, "/dev/video1")  # using V4L2
#camera = jetson.utils.gstCamera(1640, 1232, "/dev/video0") # picamera via V4L2
#camera = jetson.utils.gstCamera(3280, 2464, "0") #framerate too high
camera = jetson.utils.gstCamera(1640, 1232, "0") #framerate too high
#camera = jetson.utils.gstCamera(1920, 1080, "0") # noisy
#camera = jetson.utils.gstCamera(1280, 720, "0")
display = jetson.utils.glDisplay()

# process frames until user exits
while display.IsOpen():
	# capture the image
	img, width, height = camera.CaptureRGBA()

	# detect objects in the image (with overlay)
	detections = net.Detect(img, width, height, "box,labels,conf")

	# print the detections
	# if len(detections):
	# 	print("detected {:d} objects in image".format(len(detections)))
	# 	for detection in detections:
	# 		print(detection)

	# render the image
	display.RenderOnce(img, width, height)

	display.SetTitle("Object Detection | Network {:.0f} FPS".format(net.GetNetworkFPS()))
	# update the title bar
	display.SetTitle("{:s} | Network {:.0f} FPS".format(NETWORK, net.GetNetworkFPS()))

	# print out performance info
	#net.PrintProfilerTimes()
