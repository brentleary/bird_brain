
# sudo apt-get install libatlas-base-dev gfortran
# sudo apt-get install libhdf5-serial-dev hdf5-tools
# sudo apt-get install python3-dev
# sudo apt-get install nano locate
# sudo apt-get install libxml2-dev libxslt1-dev
# sudo apt-get install libfreetype6-dev python3-setuptools
# sudo apt-get install protobuf-compiler libprotobuf-dev openssl
# sudo apt-get install libssl-dev libcurl4-openssl-dev
# sudo apt-get install cython3
# sudo apt-get install libhdf5-serial-dev hdf5-tools libhdf5-dev zlib1g-dev zip libjpeg8-dev liblapack-dev libblas-dev gfortran
# sudo apt-get install python3-pip
# sudo apt-get install python-tk libgtk-3-dev
# sudo apt-get install libcanberra-gtk-module libcanberra-gtk3-module
# sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev
# sudo apt-get install libxvidcore-dev libavresample-dev
# sudo apt-get install libtiff-dev libjpeg-dev libpng-dev
# sudo apt-get install build-essential pkg-config
# sudo apt-get install libtbb2 libtbb-dev
# sudo apt-get install python-tk libgtk-3-dev
# sudo apt-get install libcanberra-gtk-module libcanberra-gtk3-module
# sudo apt-get install libv4l-dev libdc1394-22-dev
sudo apt-get update
sudo apt-get install git cmake
sudo pip3 install virtualenv virtualenvwrapper

# install prerequisites
sudo apt-get install libhdf5-serial-dev hdf5-tools libhdf5-dev zlib1g-dev zip libjpeg8-dev

# Latest CMAKE
wget http://www.cmake.org/files/v3.13/cmake-3.13.0.tar.gz
tar xpvf cmake-3.13.0.tar.gz cmake-3.13.0/
cd cmake-3.13.0/
./bootstrap --system-curl
make -j4
echo 'export PATH=/home/nvidia/cmake-3.13.0/bin/:$PATH' >> ~/.bashrc
source ~/.bashrc

# Install and upgrade pip3.
sudo apt-get install python3-pip
sudo pip3 install -U pip testresources setuptools

# Install the Python package dependencies.
# sudo pip3 install -U numpy==1.16.1 future==0.17.1 mock==3.0.5 h5py==2.9.0 keras_preprocessing==1.0.5 keras_applications==1.0.8 gast==0.2.2 futures protobuf pybind11                     
sudo pip3 install -U numpy==1.16.1 future==0.17.1 mock==3.0.5 h5py==2.10.0 keras_preprocessing==1.1.0 keras_applications==1.0.8 gast==0.3.3 futures protobuf pybind11
sudo pip3 install --pre --user --extra-index-url https://developer.download.nvidia.com/compute/redist/jp/v44 tensorflow
